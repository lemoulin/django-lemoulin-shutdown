==================
Le Moulin Shutdown
==================

Prevents access to a site from unauthorized users.

.. contents::

Getting started
===============

Requirements
------------

Le Moulin Shutdown requires:

- Python 3.4
- Django 1.7 or greater

Installation
------------

You can get Le Moulin Shutdown by using pip::

    $ pip install -e git+https://bitbucket.org/lemoulin/django-lemoulin-shutdown.git#egg=lemoulin_shutdown

To enable `lemoulin_shutdown` in your project you need to add it to `INSTALLED_APPS` in your projects
`settings.py` file::

    INSTALLED_APPS = (
        ...
        'lemoulin_shutdown',
        ...
    )

And add `lemoulin_shutdown.middleware.ShutdownMiddleware` to `MIDDLEWARE_CLASSES`::

    MIDDLEWARE_CLASSES = (
        ...
        'lemoulin_shutdown.middleware.ShutdownMiddleware',
        ...
    )

Usage
=====

Shuting down your site
----------------------

Add `SHUTDOWN_SITE = True` to your `settings.py` file

Ignoring URLs
-------------

You can allow access to some URLs by specifying in your `settings` file::

    `SHUTDOWN_IGNORED_URLS = (r'^myapp/$', r'^about/', )`

default is::

    `(r'^/admin/$', )`

Custom template
---------------

You can user your own template:

    `lemoulin_shutdown/shutdown.html`
