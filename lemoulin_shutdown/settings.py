from __future__ import unicode_literals

from django.conf import settings

SHUTDOWN_SITE = getattr(settings, "SHUTDOWN_SITE", False)
TEMPLATE = getattr(settings, "SHUTDOWN_TEMPLATE", "lemoulin_shutdown/shutdown.html")
IGNORED_URLS = getattr(settings, "SHUTDOWN_IGNORED_URLS", (r'^/admin/.*', ))

#FROM_DATE = getattr(settings, "SHUTDOWN_FROM_DATE", None)
#TO_DATE = getattr(settings, "SHUTDOWN_TO_DATE", None)
